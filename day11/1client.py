"""
你需要通过HTTP协议以客户端的方式访问多种服务。例如，下载数据或者与基于REST的API进行交互。
"""
from urllib import request, parse
import requests
url = 'http://httpbin.org/get'

parms = {
    'name1': 'value1',
    'name2': 'value2'
}
querystring = parse.urlencode(parms)
u = request.urlopen(url + '?'+querystring)
resp = u.read()
print(resp)
resp1 = requests.get(url)
print(resp1.text)