"""
你已经启动了一个线程，但是你想知道它是不是真的已经开始运行了。
"""


from threading import Thread, Event
import time


def countdown(n, started_evt):
    print("countdown starting")
    started_evt.set()
    while n>0:
        print('t-minus', n)
        n -= 1
        time.sleep(2)


started_evt = Event()

print('Launching countdown')
t = Thread(target=countdown, args=(10, started_evt))
t.start()

started_evt.wait()
print("countdown is running")