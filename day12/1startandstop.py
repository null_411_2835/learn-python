import time
from threading import Thread


# def countdown(n):
#     while n>0:
#         print('t-times', n)
#         n -= 1
#         time.sleep(2)
#
#
# t = Thread(target=countdown, args=(10,))
# t.start()

class CountdownTast:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n):
        while self._running and n>0:
            print('t-minus', n)
            n -= 1
            time.sleep(2)


c = CountdownTast()
t = Thread(target=c.run, args=(4,))
t.start()
c.terminate()
t.join()