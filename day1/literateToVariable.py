"""
实现解压可迭代对象赋值给多个变量
"""
from audioop import avg

# 星号表达式获取指定的数据类型
def drop_first_last(grades):
    first, *middle, last = grades
    return middle


l = [1, 2, 3]
c4 = (12, 10, 11, 13, 15, 9)
print(drop_first_last(c4))
print(sum(l))
# f, *m, l = c4
# print(f)
# print(m)
# print(l)

records = [
    ('foo', 1, 2),
    ('bar', 'baidu'),
    ('foo', 4, 5),
]


def do_foo(x, y):
    print('foo', x, y)


def do_bar(s):
    print('bar', s)


for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)

line = 'nobody:*:-2:-2:Unprivileged User:/var/empty:/usr/bin/false'
uname, *fields, homedir, sh = line.split(':')
print(uname)
print(*fields)
print(homedir)
print(sh)