import json
from collections import OrderedDict
d = OrderedDict()
d['foo'] = 1
d['bar'] = 2
d['spam'] = 3
d['grok'] = 4
for key in d:
    print(key, d[key])
print(json.dumps(d))

f = {'z': 1, 'w': 2, 'c': 3, 'r': 4}
print(f)
for key in f:
    print(key, f[key])