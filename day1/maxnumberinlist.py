"""
找出一个序列中出现次数最多的元素
"""
from collections import Counter
words = [
    'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
    'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
    'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
    'my', 'eyes', "you're", 'under'
]
morewords = ['why', 'are', 'you', 'not', 'looking', 'in', 'my', 'eyes']
word_count = Counter(words)
top_two = word_count.most_common(1)
print(top_two)
times_into = word_count['into']
print(times_into)
a = Counter(words)
b = Counter(morewords)
print(a)
print(b)
c = a + b
print(c)