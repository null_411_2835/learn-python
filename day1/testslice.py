"""
解决程序包含大量无法直视的硬编码切片
"""
items = [0, 1, 2, 3, 4, 5, 6]
a = slice(2, 4)
print(items[2:4])
print(items[a])

# indices(size)方法
s = "helloworld"
a.indices(len(s))
for i in range(*a.indices(len(s))):
    print(s[i])

