"""
通过名称来访问元素
"""
from collections import namedtuple
subscriber = namedtuple('subscriber', ['addr', 'joined'])
sub = subscriber('1099260831@qq.com', '2019-8-4')
print(sub)
print(sub.addr)
print(sub.joined)