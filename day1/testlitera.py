"""
数据序列上执行聚集函数
"""

nums = [1, 2, 3, 4, 5]
s = sum(x*x for x in nums)
print(s)