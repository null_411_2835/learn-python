"""
在一个序列上面保持元素顺序的同时消除重复的值
"""


# 序列中元素为hashable的时候才管用
def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


a = [1, 2, 4, 1, 2]
print(list(dedupe(a)))


def dedupenothashble(items, key=None):
    seen = set()
    for item in items:
        val = item if key is None else key(item)
        if val not in seen:
            yield item
            seen.add(val)


a = [{'x': 1, 'y': 2}, {'x': 1, 'y': 3}, {'x': 1, 'y': 2}, {'x': 2, 'y': 4}]
print(list(dedupenothashble (a, key=lambda d: (d['x'], d['y']))))