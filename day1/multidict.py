"""
实现一个键对应多个值的字典
"""
from collections import defaultdict
d = defaultdict(list)
d['a'].append(1)
d['a'].append(2)
d['a'].append(3)
print(d.values())

f = defaultdict(set)
f['b'].add(4)
f['b'].add(4)
f['b'].add(6)
print(f.values())
print(f)

e = defaultdict(list)
for key, value in e:
    e[key].append(value)