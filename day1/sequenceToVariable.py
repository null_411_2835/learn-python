"""
练习将序列分解为单独的变量
只要是对象是可迭代的，就可以执行分解操作，包括字符串、文件对象、迭代器、生成器
"""
p = (1, 2)
x, y = p
print(x)
print(y)

data = ['abc', 30, 90.1, (2121, 12, 11)]
name, age, high, birth, = data
print(name)
print(age)
print(high)
print(birth)

s = 'wen'
a, b, c = s
print(a)
print(b)
print(c)
