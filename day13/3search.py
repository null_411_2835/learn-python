"""
你的程序如何能够解析命令行选项
"""
import argparse

parse = argparse.ArgumentParser(description='search some files')
parse.add_argument(dest='filenames', metavar='filename', nargs='*')
parse.add_argument('-p', '--pat', metavar='pattern', required=True,
                   dest='patterns', action='append',
                   help='text pattern to search for')

args = parse.parse_args()


print(args.filenames)
print(args.patterns)