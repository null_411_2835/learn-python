"""
你想向标准错误打印一条消息并返回某个非零状态码来终止程序运行
"""
import sys
sys.stderr.write("failed\n")
raise SystemExit(1)