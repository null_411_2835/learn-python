"""
你希望在脚本和程序中将诊断信息写入日志文件。
"""
import logging


def main():
    logging.basicConfig(
        filename='app.log',
        level=logging.ERROR
    )

    hostname = 'www.python.org'
    item = 'spam'
    filename = 'data.csv'
    mode = 'r'

    logging.critical('Host %s unknown', hostname)
    logging.error('couldnt find %r', item)
    logging.warning('Feature is deprecated')
    logging.info('opening file %r, mode=%r', filename, mode)
    logging.debug('Got here')


if __name__ == '__main__':
    main()