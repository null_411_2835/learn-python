"""
你写了个脚本，运行时需要一个密码。此脚本是交互式的，因此不能将密码在脚本中硬编码， 
而是需要弹出一个密码输入提示，让用户自己输入。
"""
import getpass

user = getpass.getuser()
passwd = getpass.getpass()


def svc_login(user, passwd):
    pass


if svc_login(user, passwd):
    print('ye')
else:
    print('boo!')