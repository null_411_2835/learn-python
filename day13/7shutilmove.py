"""
你想要复制或移动文件和目录，但是又不想调用shell命令。
"""


import shutil

shutil.copy(src, dst)

shutil.copy2(src, dst)

shutil.copytree(src, dst)

shutil.move(src, dst)

