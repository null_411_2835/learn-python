"""
你想给某个函数库增加日志功能，但是又不能影响到那些不使用日志功能的程序。
"""

import logging

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


def func():
    log.critical('a critical error')
    log.debug('a debug message')