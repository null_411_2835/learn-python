"""
你想在函数上添加一个包装器，增加额外的操作处理(比如日志、计时等)。
"""
import time
from functools import wraps


def timethis(func):
    """
    计算程序运行时间
    :param func:
    :return:
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(func.__name__, end-start)
        return result
    return wrapper


@timethis
def countdown(n):
    while n > 0:
        n -= 1


countdown(10000000)
