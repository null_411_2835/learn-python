"""
学习python3运算符
python支持如下运算符：
算术运算符，比较运算符，赋值运算符，位运算符，成员运算符，身份运算符，运算符优先级
"""

# 算术运算符 + - * % ** // /
a = 10
b = 0
c = 3
print('加法运算', a+b)
print('减法运算', a-b)
print('乘法运算', a*b)
print('除法运算', a/c)
print('幂运算', a**c)
print('取模运算', a % c)
print('取整除', a//c)

# 比较运算符 == ！= > < >= <=

if a == b:
    print('a等于b')
elif a >= b:
    print('a大于等于b')
elif a != b:
    print('a不等于b')

# 赋值语句 = += -= *= /= %= **= //= :=

c = a + b
print('c=', c)
c += a
print('c=', c)
c *= a
print('c=', c)
c /= a
print('c= ', c)
c %= a
print('c=', c)
c **= a
print('c= ', c)
c //= a
print('c= ', a)


# 位运算符

c = a | a
print('c= ', c)
c2 = a & c
print('c2= ', c2)
c3 = ~a
print('c3= ', c3)

# 逻辑运算符 and or not

if a or b:
    print('a是整数')
elif c < 10:
    print('other')


#  成员运算符 in not in
l = [1, 10, 100]
if a in l:
    print('l 包含a的值')
else:
    print('l不包含a的值')


# 身份运算符 is is not
"""
is ==的区别
is用于判断两个变量引用对象是否为同一个
== 用于判断引用变量的值是否相等
"""
a1 = 3
a2 = 4
if a1 is a2:
    print('a1和a2相同')
else:
    print('他们不一样')
if id(a1) == id(a2):
    print('他们内存地址相同')
else:
    print('他们内存地址不一样')

"""
运算符优先级
从高到低如下
** 指数
~ = - 按位反转，加号 ，减号
* / % // 乘 除 求余 取整除
+- 加法减法
>><< 右移 左移
& 位
^| 位运算
< > >== <== 比较运算
== ！= 等于运算
= += -= %= /= //= *= **= 赋值运算
is is not 成员运算符
not and or 逻辑运算
"""

