"""
编码
默认以UTF-8编码，所有字符串都是unicode字符串，可以为源文件指定不同的编码
# -*- coding:cp-1252 -*-

标识符
第一个字符必须是字母或者下划线
标识符其他部分由字母、数字、下划线组成
标识符对大小写敏感
"""
import keyword
print(keyword.kwlist)

word = '字符串'
sentence = "这是一个句子。"
paragraph = """这是一个段落，
由很多行组成
"""
print(word)
print("*"*20)
print(sentence)
print("*"*20)
print(paragraph)

print('hello\nword text')
print(r'hello\nword make')

name = input("\n\n请输入你的名字，按下enter键后退出:")
print(name)

