# a, b = input().split(' ')
# c, d = map(int, input().split(' '))
# print(type(a))
# print(b)
# print(type(c))
# print(d)
# f = 123.456
# print("{}".format(f))
# print("Name Age Gender")
# print("-"*21)
# print("Jack 18 man")
# b = oct(10)
# d = bin(12)
# # a = int(b, 2)
# c = int(b, 8)
# # d = int(b, 16)
# print(c)
# l = [99, 45, 78, 67, 72, 88]
# print(sorted(l, reverse=True))
# d = sorted(l, reverse=True)
# print(' '.join(d))
# import calendar
# from calendar import calendar
# print(calendar.calendar(2008, 2))
# a, b, c=input().split()
# for i in range(1, 5):
#     print("* "*4)

# nums = [5,7,7,8,8,10]
# target = 8
# print(nums.count(target))

# s = "abaccdeff"
# for i in s:
#     if s.count(i) == 1:
#         print(i)
# l = [0,1,2,3,4,5,6,7,9]
# n = len(l)
# # print(n)
# for i in range(n):
#     if l[i]-l[i-1] > 1:
#         print(i)

# nums = [1,2,3,4]
# l = []
# o = []
# for i in nums:
#     if i%2==0:
#         l.append(i)
#     else:
#         o.append(i)
# print(o+l)
# l=[1,2]
# s = " the sky  is blue "
# print(s.strip())
# print(s.split(" "))
# print(s.split())
# print(s[::-1])
# print(" ".join(s.split(" ")[::-1]))

# n = "00000000000000000000000000001011"
# print(n.count("1"))

# nums = [4,1,4,6]
# set_nums = list(set(nums))
# print(nums)
# l = []
# for i in set_nums:
#     # print(i)
#     if nums.count(i)==1:
#         l.append(i)
# print(l)

# a = 1
# b = 1
# l = []
# # c = [a,b]
# c = l.append(a,b)
# c = l.append(b)
# print(c)
# print(sum(c))

# l = [1, 2, 3, 2, 2, 2, 5, 4, 2]
# m = len(l)/2
# # print(m)
# for i in l:
#     if l.count(i)>m:
#         print(i)
# n = l.count(2)
# print(n)

# n = 824883294
# l  = []
# s = ""
# for i in range(1, n+1):
#     # if str(i).count(1)!=0:
#     #     sum++
#     # l.append(str(i))
#     s+=str(i)
# print(l)
# s="".join([l])
# print("".join())
# print(s.count("1"))
# l = [7, 5, 6, 4]
"""
动态规划解最长连续子子字符串
"""
# nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
# for i in range(1, len(nums)):
    # print(nums[i])
    # nums[i] += max(nums[i-1], 0)
    # print(max(nums[i-1], 0))
    # print('{},{}'.format(max(nums[i-1], 0), nums[i]))
    # print(nums[i])
    # print(max(nums))

"""
暴力法
"""
# nums = [2,7,11,15]
# target = 9
# i, j = 0, len(nums)-1
# while i<j:
#     s=nums[i]+nums[j]
#     if s>target:
#         j-=1
#     elif s<target:
#         j+=1
#     else: print("{}{}".format(nums[i],nums[j]))

"""
剑指 Offer 17. 打印从 1 到最大的 n 位数
10 100 1000  10000
"""
# n = 2
# l=[]
# for i in range(1, 10**n):
#     l.append(i)
#
# print(l)

"""
数组中的重复数字
"""
# l = [2, 3, 1, 0, 2, 5, 3]
# s = set(l)
# # print(s)
# for i in s:
#     if l.count(i)>1:
#         print(i)
#
# s = "abc"
# for i in s:
#     print(i)

# class Solution:
#     def sum_num(self, num:int):
#         """
#         求和num
#         """
#         snum = str(num)
#         total=0
#         for i in snum:
#             total+=int(i)
#         return total
#
#     def addDigits(self, num: int) -> int:
#         # if num<10:
#         #     return num
#         # else:
#         #     if num == 10:
#         #         return 1
#         if self.sum_num(num)<10:
#             return self.sum_num(num)
#         while self.sum_num(num) >= 10:
#             num = self.sum_num(num)
#             if self.sum_num(num)<10:
#                 return self.sum_num(num)

# s = Solution()
# print(s.sum_num(1))
# print(s.addDigits(10))


# 0  1  1 2 3 5....
# def fib(n):
#     if n == 1:
#         return 0
#     if n == 2:
#         return 1
#     else:
#         return fib(n-1) + fib(n-2)
#
#
# print(fib(6))


# title = "First leTTeR of EACH Word"
# l = []
# for i in title.split():
#     if len(i)>2:
#         l.append(i.capitalize())
#     else:
#         l.append(i.lower())
# print(" ".join(l))

# word = "FlaG"
# if word.isupper() or word.islower():
#     print("True")
# elif word[0].isupper() and word[1:].islower():
#     print("True")
# else:
#     print("flase")
"""
items[i] = [typei, colori, namei]
ruleKey == "type" 且 ruleValue == typei 。
ruleKey == "color" 且 ruleValue == colori 。
ruleKey == "name" 且 ruleValue == namei 。
"""
# items = [["phone","blue","pixel"],
#          ["computer","silver","lenovo"],
#          ["phone","gold","iphone"]]
# ruleKey = "color"
# ruleValue = "silver"
# sum = 0
# for i in items:
#     print(i)
#     if ruleKey=="color":
#         if i[1]==ruleValue:
#             sum+=1
#     print(sum)
#     if ruleKey=="type":
#         if i[0]==ruleValue:
#             sum+=1
#     # print(sum)
#     if ruleKey=="name":
#         if i[2]==ruleValue:
#             sum+=1
    # print(sum)


# class Solution:
#     def countMatches(self, items, ruleKey, ruleValue):
#         sum=0
#         for i in items:
#             print(i)
#             if ruleKey=="type":
#                 if i[0]==ruleValue:
#                     sum+=1
#             if ruleKey == "color":
#                 if i[1] == ruleValue:
#                     sum += 1
#             if ruleKey=="name":
#                 if i[2]==ruleValue:
#                     sum+=1
#         return sum
# c = Solution()
# print(c.countMatches(items, ruleKey, ruleValue))


# word = "abcdefd"
# ch = "d"
# if ch not in word:
#     print(word)
# else:
#     id = word.index(ch)+1
#     # print(reversed(word[0:id]))
#     print(word[0:id][::-1]+word[id:])

# sentences = ["alice and bob love leetcode", "i think so too", "this is great thanks very much"]
# l=[]
# for i in sentences:
#     # print(i.split())
#     # print(len(i.split()))
#     l.append(len(i.split()))
# print(max(l))

# class Solution:
#     def findLucky(self, arr) -> int:
#         sarr=set(arr)
#         l = []
#         for i in sarr:
#             if arr.count(i)==i:
#                 l.append(i)
#         if len(l)>=1:
#             return max(l)
#         else:
#             return -1
#
#         # return -1
#
# s = Solution()
# print(s.findLucky([3]))

#句子排序
# s = "is2 sentence4 This1 a3"
# l=[]
# for i in s.split():
#     # print(i[-1]+i[:-1])
#     l.append(i[-1]+i[:-1])
# # print(sorted(l))
# ll=[]
# for j in sorted(l):
#     ll.append(j[1:])
# print(" ".join(ll))

# 返回最大奇数
# num = "52"
# l = []
# if int(num)%2!=0:
#     print(num)
# elif int(num)%2==0:
#     for i in num:
#         if int(i)%2!=0:
#             l.append(i)
#     print(l)
# else:
#     print("")
# for i in range(len(num)):
#     l.append(num[i])
# print(l)


# def get_zero(num1,num2):
#     sum=0
#     while num1 != 0 and num2 != 0:
#         if num1 >= num2:
#             num1 -= num2
#         else:
#             num2 -= num1
#         sum += 1
#     return sum
#
#
# print(get_zero(10, 10))

# brackets = [[3,50],[7,10],[12,25]]
# income = 10
#
# for i in brackets:
#     print(i)

# s1 = "l|*e*et|c**o|*de|"
# s2 = "yo|uar|e**|b|e***au|tifu|l"

# print(s.count("|"))
# l=s1.count("|")/2
# s=""
# # print(l)
# print(s1.split("|"))
# print(s2.split("|"))
# print(s1.split("|")[int(l)].count("*"))
# for i in s1.split("|")[::2]:
#     print(i)
# for i in s2.split("|")[::2]:
#     s+=i
# print(s.count("*"))

# class Solution:
#     def countAsterisks(self, s: str) -> int:
#         s=""
#         for i in s.split("|")[::2]:
#             s+=i
#         return s

#//判断一个数的数字计数是否等于数位的值
# num = "030"
# for i in range(len(num)):
#     # print(num[i])
#     # print("==================")
#     # print(num.count(str(i)))
#     if int(num[i])==num.count(str(i)):
#         print("true")
#     else:
#         print("false")


# 第一个出现两次的字母

# s = "abccbaacz"
# for i in s:
#     print(i)
    # print("i={},index={}".format(i, s.index(i)))


# num = 2932
# for i in str(num):
#     print(i)

# nums1 = [1,2,3,3]
# nums2 = [1,1,2,2]
# l = [[],[]]
# answer=[]
# # answer[0] = []
# # answer[1] = []
# # print(answer)
# for i in nums1:
#     if i not in nums2:
#         l[0].append(i)
# for j in nums2:
#     if j not in nums1:
#         l[1].append(j)
# print(l)

# 找出两数组的不同
# class Solution:
#     def findDifference(self, nums1, nums2):
#         l = [[],[]]
#         for i in nums1:
#             if i not in nums2:
#                 l[0].append(i)
#         for j in nums2:
#             if j not in nums1:
#                 l[1].append(j)
#         return l
#
# s = Solution()
# s.findDifference(nums1,nums2)

# s = "11111222223"
# k=3
# while len(s)>k:

# sentence = "this problem is an easy problem"
# searchWord = "pro"
# l=[]
# for i in sentence.split():
#     print(i)
#     if searchWord in i:
#         l.append(i)
#         # print(sentence.split().index(i)+1)
# print(l)
# print(sentence.split().index(l[0])+1)

# 移除指定数字得到的最大结果
# number = "123"
# digit = "1"
# print(number.removesuffix(digit))
# print(number.removesuffix(digit))

# 作为子字符串出现在单词中的字符串数目
# patterns = ["a","abc","bc","d"]
# word = "abc"
# sum = 0
# for i in patterns:
#     if i in word:
#         sum+=1
# print(sum)

# s = "1 box has 3 blue 4 red 6 green and 12 yellow marbles"
# l=[]
# ll=[]
# for i in s.split():
#
#     if i.isdigit():
#
#         l.append(i)
# ll = sorted(int(l))
# print(l[4])
# print(l[3])
# if int(l[4])>int(l[3]):
#     print(l[4])
# print(ll)
# s=0
# print(len(l))
# for i in range(1, len(l)):
#     print("i={}".format(i))
#     if int(l[i])>int(l[i-1]):
#         print(l[i])
#         # ll.append(l[i])
#         s+=1
#
#
# print("s={}".format(s))
# if l==ll:
#     print("true")
# else:
#     print("false")

# sn = str(n)
# sum = 0
# if n<9:
#     print("false")
# else:
#     sum=0
#     for i in str(n):
#         sum+=int(i)**2
# print(sum)
# n = 19
# sum=0
# while n!=1:
#     for i in str(n):
#         sum+=int(i)**2
#     n=sum
#     print(n)
# print(n)
# nums = [1,1,0,1,1,1]
# # n = ",".join(nums)
# sum = ''
# for i in nums:
#     sum+=str(i)
# # print(max(len(sum.split("0"))))
# print(sum.split('0'))
# for i in sum.split('0'):
#     print(len(i))
# n=32
# while n>2:
#     n/=2
#     print(n)
# s = "anagram"
# t = "nagaram1"
# sum=0
# for i in set(s+t):
#     print(i)
#     if s.count(i)==t.count(i):
#         sum+=1
# print(sum)
# n=6
# l=[]
# a=b=c=0
# a==n/2
# l.append(a)
# b==n/3
# l.append(b)
# c=n/5
# l.append(c)
# print(l)

# num=[9,6,4,2,3,5,7,0,1]
# n=len(num)
# print(n)
# for i in range(n+1):
#     print(i)
#     if i not in num:
#         print("lost is {}".format(i))

# n = 4
# if n<4:
#     print("true")
# else:
#     while n>7:
#         if
#     print("false")

# pattern = "aaaa"
# s = "dog cat cat dog"
# print(list(pattern))
# print(list(s.split()))
# # d = {key:value for key,value in zip(list(pattern), list(s.split()))}
# d = dict(zip(list(pattern),list(s.split())))
# print(d)

# n=1
# # if n%3!=0:
# #     print("false")
# while n >= 3:
#     n/=3
# if str(float(n))=="1.0":
#     print(str(float(n)))
#     print("true")
# else:
#     print("flase")

# class Solution:
#     def isPowerOfThree(self, n: int) -> bool:
#         # if n%3!=0:
#         #     return False
#         while n>=3:
#             n/=3
#         if str(float(n))=="1.0":
#             print(str(float(n)))
#             return True
#         else:
#             return False
#
# s=Solution()
# print(s.isPowerOfThree(1))

# nums1 = [1,2,2,1]
# nums2 = [2,2]
# l=[]
# for i in nums1:
#     if i in nums2:
#         l.append(i)
# l = set(l)
# print(list(l))
# class Solution:
#     def intersection(self, nums1, nums2):
#         l=[]
#         for i in nums1:
#             if i in nums2:
#                 l.append(i)
#         return l
# s = Solution()
# print(s.intersection(nums1, nums2))

# 超大测试用例无法测试通过
# nums = [1,1]
# n = len(nums)
# print(n)
# # print(range(0,n))
# l = []
# for i in range(1,n+1):
#     if i not in nums:
#         l.append(i)
# print(l)


# s = "Hello, my name is John"
# for i in s.split():
#     if i.isalpha():
#         print(i)

# thirdmax number
# num = [1,2]
# num=set(num)
# ll = sorted(list(num))
# print(ll[::-1][2])

#
# s = "abcdefg"
# k = 1
# print(s[:2 * k][0:k][::-1]+s[k:2*k])
# if len(s)-2*k<k:
#     print(s[2*k:][::-1])
# elif len(s)-2*k<2*k and len(s)-2*k>=k:
#     print(s[2*k:][0:k][::-1]+s[2*k+k:])
# else:
#     print(s)

# s = "a"
# t = "aa"
# for i in t:
#     if i not in s:
#         print(i)
#     elif s+s==t:
#         print(s)

# s = "Hello"
# print(s.lower())

# nums = [0]
# l = []
# ll = []
# for i in nums:
#     if i%2==0:
#         l.append(i)
#     else:
#         ll.append(i)
# print(l)
# print(ll)
# print(l+ll)

# nums = [1,6,2,3]
# sorted_nums=sorted(nums)
# print(sorted_nums)
# if nums==sorted_nums or nums==sorted_nums[::-1]:
#     print("true")
# else:
#     print("false")

# allowed = "ab"
# words = ["ad","bd","aaab","baa","badab"]
# sum=0
# s=0
# for i in words:
#     for j in i:
#         if j in allowed:
#             sum+=1
#     if sum==len(i):
#         s+=1
# print(s)
# l = []
# s1 = "d b zu d t"
# s2 = "udb zu ap"
# for i in s1.split():
#     if i not in s2 and s1.count(i)==1:
#         l.append(i)
# for j in s2.split():
#     if j not in s1 and s2.count(j)==1:
#         l.append(j)
# print(l)
# words = ["bella","label","roller"]
# l = []
# for i in range(len(words)):
#     for j in words[i]:
#         if [j in k for k in words]:
#             l.append(j)
# print(l)

# nums = [555,901,482,1771]
# s=0
# for i in nums:
#     if len(str(i))%2==0:
#         s+=1
# print(s)

# num = 1800
# revser1 = str(num)[::-1]
# print(int(revser1))
# revser2 = revser1[::-1]
# print(revser2)
# if int(revser2)==int(num):
#     print("true")
# else:
#     print("false")

# word1 = "abcdeef"
# word2 = "abaaacc"
# s = set(word2+word1)
# print(s)
# l1 = []
# l2= []
# l = []
# for i in s:
#     l1.append(word1.count(i))
#     l2.append(word2.count(i))
# for i in range(len(l1)):
#     l.append(abs(l1[i]-l2[i]))
# print(l)

# s = "abbcccddddeeeeedcba"
# # strr=''
# for i in range(len(s)-1):
#     if s[i]==s[i+1]:
#         strr=s[i]
#         strr+=s[i]
# print(strr)
# num = "35427"
# l = []
# if int(num)%2!=0:
#     print(num)
# else:
#     for i in num:
#         if int(i)%2!=0:
#             l.append(i)
# if len(l)==0:
#
# print(sorted(l)[-1])

# num = "10133890"
# class Solution:
#     def largestOddNumber(self, num):
#         l=[]
#         if int(num)%2!=0:
#             return num
#         else:
#             for i in num:
#                 if int(i)%2!=0:
#                     l.append(i)
#             if len(l)==0:
#                 return ""
#             else:
#                 return sorted(l)[-1]
# s = Solution()
# print(s.largestOddNumber(num))

# 十进制整数的反码
n = 5
bn = bin(5)
print(bn)
l = []
for i in str(bn[2:]):
    print(i)







