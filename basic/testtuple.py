"""
练习元组
元组与列表类似，不同之处在于元组的元素不能修改
元组使用小括号（），列表使用方括号[]
"""
tup1 = ('baidu', 'tencent', 'google')
tup2 = (1, 2, 3)
print(type(tup1))
tup3 = (14)
tup4 = (14, )
print(type(tup3))
print(type(tup4))
print(tup1[0])
print(tup1[1:3])

# 元组不能修改修改，但是可以对元组进行连接组合
# tup2[1] = 5
# print(tup2)
tup5 = tup1 + tup4
print(tup5)

# 元素值不允许删除，但是可以使用del语句删除整个元组, 之后会抛出异常信息
# del tup4
# print(tup4)

# 元组运算符 len（） + 复制 迭代 判断元素是否存在
print(len(tup1))
print(tup4*2)
if 'baidu' in tup1:
    print(True)
print('baidu' in tup1)
for i in tup1:
    print(i)

# 元组内置函数 len() max() min() tuple()

# 元组的不可变是元组所指向的内存中的内容不变
tup6 = (1,)
print(id(tup6))
tup6= (1, 2)
print(id(tup6))