var = 'hello kuka'
age = 'you age is ten'
print(var)
print(var[0])
print(var[0:])
print(var[-1])
print(var[:-1])
print(var[::-1])
print(var+" "+age)
print('\a')
# 字符串格式化
print('my name is %s, i am %d years old' % ('mali', 98))

para_str = """多字符串实列
多行字符串可以使用制表符
TAB（\t）
也可以使用换行符[\n]
"""
print(para_str)

# f-string 格式化字符
w = {'name': 'rubi', 'url': 'www.rubi.com'}
print(f'{w["name"]}')
print(f'{w["url"]}')

# 字符串内建函数
print(var.capitalize())
print(var.center(20, '&'))
print(var.find('k'))