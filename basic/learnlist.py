"""
学习列表
"""

# 访问列表中的值
list = ['wangwu', 'china', 'japan', 'air']
print(list[0])
print(list[3])
print(list[-2])
print(list[0:2])
list.append('taiwan')
print('追加台湾之后的列表', list)
list[0] = 'guangzhou'
print('更新列表元素：', list)
del list[-1]
print('删除最后一个元素之后的列表：', list)

# 列表对+ 和* 的操作符与字符串相类似
print(list+list)
print(list*3)

# 列表的函数与方法
print(len(list))
print(max(list))
print(min(list))

print(list.append('beijing'))
print(list.copy())
print(list.count('china'))
l = ['abc', 'edf']
print(list.extend(list)) # todo 搞清楚这个函数的用法
print(list.index('air'))
print(list.insert(2, 'indie'))
print(list.pop())
l.clear()
print(l)

#####################################
a, b = input().split(' ')
c, d = map(int, input().split(' '))
print(a)
print(b)
print(c)
print(d)