"""
python 提供了time和calendar模块可以用于格式化日期和时间，其他模块包括datetime,pytz,dateutil
"""
import time
# todo 导入查找时间模块报错
# import calendar
now = time.time()
print('now = {}'.format(now))
localtime = time.localtime()
print('localtime = {}'.format(localtime))
prettytime = time.asctime()
print('prettytime = {}'.format(prettytime))
# 格式化日期
print(time.strftime("%Y-%m-%d %H:%m:%S"))
# 获取某月日历
# cal = calendar.month(2021, 6)
# print("六月的日历是：%d" % cal)
# print(cal)
