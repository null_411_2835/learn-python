"""
练习python3基本数据类型
python3六种标准数据类型：数字，字符串，列表，元组，集合，字典
不可变数据：数字，字符串，元组
可变数据：列表，字典，集合
"""
counter = 100       # 整型
miles = 1000.0      # 浮点型
name = 'dear'       # 字符串

print(counter)
print(miles)
print(name)

print(type(counter))
print(type(miles))
print(type(name))

"""
数字：int float bool complex
"""
a, b, c, d = 10, 2.3, True, 4+1j
print(type(a), type(b), type(c), type(d))
print(isinstance(b, int))

"""
字符串：以单引号或者双引号括起来
"""
str = 'china'

print(str)
print(str[1])
print(str*3)
print('i love ' + str)
print(str[0:])
print(str[-1])

"""
列表 python中使用最频繁的数据类型
"""

list1 = ['123', 'abc', 33, 90.9]
list2 = [12, 'japan']
print(list1)
print(list1[0])
print(list1[1:3])
print(list1+list2)


"""
元组，元素不能改变
"""

tuple1 =('123', 'abc', 33, 90.9)
tuple2 =(12, 'japan')
print(tuple1)
print(tuple1[0])
print(tuple1[1:3])
print(tuple1+tuple2)


"""
集合：由一个或数个形态各异的大小整个组成，可以使用大括号{}或者set（）函数创建集合
"""

sites = {'baidu', 'sina', 'zhihu', 'facebook'}
print(sites)
a = set('abc')
b = set('bcd')
print(a)
print(b)
print(a-b)
print(a|b)
print(a&b)
print(a^b)

"""
字典：key：value
"""

tinydict = {'name': 'fireman', 'age': '45', 'site': 'www.baidu.com'}
print(tinydict)
# print(tinydict[2])
print(tinydict.keys())
print(tinydict.values())
print(tinydict.get('age'))
