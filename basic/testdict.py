"""
字典可存储任意类型对象使用{}
"""
dict = {'name': 'robbin', 'likes': 'apple', 'url': 'www:baidu.com'}
print(dict['likes'])
print(dict.get('url'))
dict['likes'] = 'eggs'
print(dict.get('likes'))
# dict.clear()
# print(dict)
# del dict
# print(dict)

"""
字典特性
1. 不允许同一个键出现两次，创建时如果同一个键被赋值两次，后一个值会被记住
2. 键必须不可变，所以用数字，字符串或元组充当，而用列表就不行
"""

# 字典内置函数 len() str() type()
print(len(dict))
print(str(dict))
print(type(dict))

# 字典内置方法 clear() copy() fromkeys() get() key in dict items() keys() setdefault() update() values() pop() popitem()
print(dict.copy())
print(dict.fromkeys(dict))
print(dict.get('name'))
print(dict.setdefault('name', __default=None))