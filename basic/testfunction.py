"""
函数是有组织的，可重复使用的，用来实现单一，或者相关功能的代码段
"""


def printme(name):
    print("ni hao" + " " + name)
    return

printme("wenmingwei")

# 参数传递

# 传递不可变对象实列


def ChangeInt(a):
    a = 10


b = 2
ChangeInt(b)
print(b)

# 传递可变对象


def changeme(mylist):
    mylist.append([1,2,3])
    print(mylist)
    return

mylist = [10,20]
changeme(mylist)
print(mylist)


# 必备参数


def printmemust(str):
    print(str)

printmemust("123")


# 关键字参数
def printmekeyword(str):
    print(str)


printmekeyword(str="ni hao kelly")


# 默认参数


def printinfo(name, age = 50):
    print("you name is {}, age is {}".format(name, age))


printinfo("wen")


# 不定长参数

def printvar(arg, *vararg):
    print(arg)
    for var in vararg:
        print(var)

printvar(20)
printvar(10, 20, 30)


# 匿名函数lambda


sum = lambda arg1, arg2:arg1+arg2
print(sum(10, 90))


# 全局变量和局部变量