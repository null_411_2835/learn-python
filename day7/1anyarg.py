"""
你想构造一个可接受任意数量参数的函数
"""


def avg(first, *rest):
    return (first + sum(rest))/(1+len(rest))


print(avg(1, 2))
print(avg(1, 2, 3, 4))


def anyargs(*args, **kwargs):
    print(args)
    print(kwargs)


anyargs(1, 2, 3, 4, {"age": 12}, size = 'large')