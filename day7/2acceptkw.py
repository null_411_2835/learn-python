"""
你希望函数的某些参数强制使用关键字参数传递
"""


def recv(maxsize, *, block):
    """
    :param maxsize:
    :param block:
    :return: nothing
    """
    pass


# recv(1024, block)
recv(1024, block=False)


def mininum(*values, clip=None):
    m = min(values)
    if clip is not None:
        m = clip if clip > m else m
    return m


print(mininum(1, 3, -5, 10))
print(mininum(1, 3, -5, 10, clip=0))
print(help(recv))
