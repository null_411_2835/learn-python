"""
你用lambda定义了一个匿名函数，并想在定义时捕获到某些变量的值。
"""
# todo 匿名函数捕获变量值不太理解
x = 10
a = lambda y: x+y
x = 20
b = lambda y: x+y
print(a(10))
print(b(10))