"""
你有一个除 __init__() 方法外只定义了一个方法的类。为了简化代码，你想将它转换成一个函数。
"""
from urllib .request import urlopen


class UrlTemplate:
    def __init__(self, template):
        self.template = template

    def open(self, **kwargs):
        return urlopen(self.template.format_map(kwargs))


yahoo = UrlTemplate('https://www.baidu.com/s?wd={names}')
for line in yahoo.open(names='123', fields=''):
    print(line.decode('utf-8'))