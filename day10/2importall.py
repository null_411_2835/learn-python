"""
当使用’from module import *’ 语句时，希望对从模块或包导出的符号进行精确控制。
"""


def spam():
    pass


def grok():
    pass


blah = 39


__all__ = ['spam', 'grok']

