"""
你想在字节字符串上执行普通的文本操作(比如移除，搜索和替换)。
"""

data = b'Hello World'
print(data)
print(data.split())