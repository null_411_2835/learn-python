"""
想在字符串中搜索和匹配指定的文本模式
"""
import re
text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
print(text)
print(re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text))

datapt = re.compile(r'(\d+)/(\d+)/(\d+)')
# print(re.sub(datapt, r'\3-\1-\2', text))
print(datapt.sub(r'\3-\1-\2', text))