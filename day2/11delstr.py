"""
你想去掉文本字符串开头，结尾或者中间不想要的字符，比如空白。
"""
s = ' hello world \n'
print(s)
print(s.strip())
print(s.rstrip())
print(s.lstrip())
t = '-----hello====='
print(t)
print(t.strip('-'))
print(t.strip('='))

s1 = ' hello     world \n'
print(s1)
print(s1.replace(' ', ''))