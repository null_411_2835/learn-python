"""
想匹配或者搜索特定模式的文本
"""
import re
text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
datapt = re.compile(r'\d+/\d+/\d+')
print(datapt.findall(text))
datapt1 = re.compile(r'(\d+)/(\d+)/(\d+)')
m = datapt1.match('2999/12/21')
year, month, day = m.groups()
print(year, month, day)
for month, day, year in datapt1.findall(text):
    print('{}-{}-{}'.format(year, month, day))


