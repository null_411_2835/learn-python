"""
你正在试着使用正则表达式去匹配一大块的文本，而你需要跨越多行去匹配。
"""
import re
common = re.compile(r'/\*(.*)\*/')
text1 = '/* this is a comment */'
text2 = '''/* this is a
    multiline comment */
 '''
print(common.findall(text1))
print(common.findall(text2))

commons = re.compile(r'/\*(.*)\*/', re.DOTALL)
print(commons.findall(text2))