"""
你想通过某种对齐方式来格式化字符串
"""
text = 'Hello World'
print(text)
print(text.rjust(20))
print(text.rjust(20, '*'))
print(text.ljust(20))
print(text.center(20))

print(format(text, '%>30'))
print(format(text, '<30'))
print(format(text, '^30'))