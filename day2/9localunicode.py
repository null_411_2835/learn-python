"""
你正在处理Unicode字符串，需要确保所有字符串在底层有相同的表示。
"""
import unicodedata
s1 = 'Spicy Jalape\u00f1o'
s2 = 'Spicy Jalapen\u0303o'
t1 = unicodedata.normalize('NFC', s1)
t2 = unicodedata.normalize('NFC', s2)
if t1 == t2:
    print("true")
print(''.join(c for c in t1 if not unicodedata.combining(c)))