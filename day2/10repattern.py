"""
你正在使用正则表达式处理文本，但是关注的是Unicode字符处理。
"""
import re
pat = re.compile('stra\u00dfe', re.IGNORECASE)
s = 'straße'
print(s)
print(pat.match(s))
print(pat.match(s.upper()))
print(s.upper())