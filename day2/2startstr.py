"""
你需要通过指定的文本模式去检查字符串的开头或者结尾，比如文件名后缀，URL Scheme等等。
"""
url = 'http://baidu.com'
print(url.startswith("http"))
print(url.endswith("txt"))