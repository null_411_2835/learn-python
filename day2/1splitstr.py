"""
你需要将一个字符串分割为多个字段，但是分隔符(还有周围的空格)并不是固定的。
"""
import re
line = 'asdf fjdk; afed, fjek,asdf, foo'
print(re.split(r'[;,\s]\s*', line))