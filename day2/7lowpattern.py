"""
你正在试着用正则表达式匹配某个文本模式，但是它找到的是模式的最长可能匹配。 而你想修改它变成查找最短的可能匹配。
"""
import re
datapat = re.compile(r'\"(.*)\"')
text1 = 'Computer says "no."'
text2 = 'Computer says "no." Phone says "yes."'
print(datapat.findall(text1))
print(datapat.findall(text2))
datapat1 = re.compile(r'\"(.*?)\"')
print(datapat1.findall(text1))
print(datapat1.findall(text2))