"""
你想使用操作类文件对象的程序来操作文本或二进制字符串。
"""
import io

s = io.StringIO()
s.write('hello world\n')
print(s.write('Hello World\n'))
print('this is  a test', file=s)
print(s.getvalue())
print(s.read(4))
