"""
你需要将一个Python对象序列化为一个字节流，以便将它保存到一个文件、存储到数据库或者通过网络传输它。
"""
import pickle
f = open('123.txt', 'wb')
pickle.dump([1,2,3], f)
pickle.dump(['abd','cdf'], f)
f.close()
f = open('123.txt', 'rb')
print(pickle.load(f))
print(pickle.load(f))