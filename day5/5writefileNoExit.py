"""
你想像一个文件中写入数据，但是前提必须是这个文件在文件系统上不存在。 也就是不允许覆盖已存在的文件内容。
"""
# import os
# if not os.path.exists('testio1.txt'):
#     with open('testio1.txt','wt') as f:
#         f.write('ni hao\n')
# else:
#     print("file is already exists!")

with open('testio2.txt', 'xt') as f:
    f.write('ni hao ya')