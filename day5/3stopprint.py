"""
使用print函数输出数据，但是想改变默认的分隔符或者行尾符
"""
print('acb', 50, 91.7)
print('acb', 50, 91.7, sep=',')
print('acb', 50, 91.7, end='!!\n')
for i in range(5):
    print(i)

for  i in range(5):
    print(i, end='\n')

row = ('acb', 50, 91.7)
print(','.join(str(x) for x in row))
print(row, sep=',')
print(*row, sep=',')