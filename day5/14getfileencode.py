"""
你想使用原始文件名执行文件的I/O操作，也就是说文件名并没有经过系统默认编码去解码或编码过。
"""
import os
import sys

print(sys.getfilesystemencoding())
print(os.listdir('.'))