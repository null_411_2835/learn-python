"""
你想测试一个文件或目录是否存在。
"""
import os
import time

print(os.path.exists('testzip.gz'))
print(os.path.isfile('testzip.gz'))
print(os.path.isdir('testzip.gz'))
print(os.path.getctime('testzip.gz'))
print(time.ctime(os.path.getctime('testzip.gz')))