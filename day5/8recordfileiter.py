"""
你想在一个固定长度记录或者数据块的集合上迭代，而不是在一个文件中一行一行的迭代
"""
from functools import partial
RECORD_SIZE = 32
print(RECORD_SIZE)
with open('testio1.txt', 'rb') as f:
    records = iter(partial(f.read, RECORD_SIZE), b'')
    print(records)
    for r in records:
        print(r)