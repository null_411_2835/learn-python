"""
将print（）函数的输出重定向到一个文件中去
"""
with open('./testio.txt', 'at') as f:
    print("ni hao ", file=f)

with open('./testio.txt', 'rt') as f:
    for line in f:
        print(line)