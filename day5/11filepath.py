"""
你需要使用路径名来获取文件名，目录名，绝对路径等等
"""
import os

path = os.path.abspath('testio2.txt')
print(os.path.exists('testio1.txt'))
print(os.path.basename('testio.txt'))
print(os.path.abspath('testzip.gz'))
print(os.path.basename(path))
print(os.path.join('tmp', 'data', os.path.basename(path)))
print(os.path.splitext(path))