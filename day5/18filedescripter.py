"""
你有一个对应于操作系统上一个已打开的I/O通道(比如文件、管道、套接字等)的整型文件描述符， 你想将它包装成一个更高层的Python文件对象。
"""
import sys
bstdout = open(sys.stdout.fileno(), 'wb', closefd=False)
bstdout.write(b'hello world\n')
bstdout.flush()