"""
你想获取文件系统中某个目录下的所有文件列表。
"""
import glob
import os
from fnmatch import fnmatch

path = r'D:\code\gitee\LearnPython\day5'

print(os.listdir(path))

txtfile1 = glob.glob('*.txt')
print(txtfile1)
txtfile2 = [name for name in os.listdir(path) if fnmatch(name, '*.txt')]
print(txtfile2)