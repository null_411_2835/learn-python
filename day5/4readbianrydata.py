"""
读写二进制文件，比如图片、声音文件等等
"""
with open('./testio.txt', 'wb') as f:
    f.write(b'bzl')
with open('./testio.txt', 'r') as f:
    print(f.read())

t = 'hello jump'
for c in t:
    print(c)
t = b'hello jump'
for c in t:
    print(c)
