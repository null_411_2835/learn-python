"""
想读写一个gzip或bz2格式的压缩文件
"""
import gzip, bz2
with gzip.open('testzip.gz', 'wt') as f:
    f.write("gzip test ")
with gzip.open('testzip.gz', 'rt') as f:
    print(f.read())
with bz2.open('testbz2.bz2', 'wt') as f:
    f.write('test bz2')
with bz2.open('testbz2.bz2', 'rt') as f:
    print(f.read())