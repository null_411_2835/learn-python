"""
你想在文本模式打开的文件中写入原始的字节数据。
"""
import sys
# sys.stdout.write(b'hello\n')
sys.stdout.buffer.write(b'hello\n')