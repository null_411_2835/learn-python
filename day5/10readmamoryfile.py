"""
你想内存映射一个二进制文件到一个可变字节数组中，目的可能是为了随机访问它的内容或者是原地做些修改。
"""
import os
import mmap


def memory_map(filename, access=mmap.ACCESS_WRITE):
    size = os.path.getsize(filename)
    fd = os.open(filename, os.O_RDWR)
    return mmap.mmap(fd, size, access=access)
size = 1000
with open('testio1.txt', 'wb') as f:
    f.seek(size-1)
    f.write(b'\x00')
m = memory_map('testio1.txt')
print(len(m))
print(m[0:11])