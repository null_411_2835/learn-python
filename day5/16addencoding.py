"""
你想在不关闭一个已打开的文件前提下增加或改变它的Unicode编码。
"""
import urllib.request
import io

u = urllib.request.urlopen('http://www.python.org')
f = io.TextIOWrapper(u, encoding='utf-8')
text = f.read()
print(text)