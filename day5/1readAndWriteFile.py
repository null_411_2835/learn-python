"""
需要读写各种不同编码的文本数据
"""
import time
with open('./testio.txt', 'wt') as f:
    text = time.time()
    f.write(str(text))
    print(' ', end='\n')

with open('./testio.txt', 'rt') as f:
    for line in f:
        print(line)