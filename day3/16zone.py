"""
你有一个安排在2012年12月21日早上9:30的电话会议，地点在芝加哥。 而你的朋友在印度的班加罗尔，那么他应该在当地时间几点参加这个会议呢？
"""
from datetime import datetime

import pytz
from pytz import timezone
d = datetime(2021, 10, 21, 9, 30, 0)
print(d)
centel_date = timezone('US/Central')
loc_date = centel_date.localize(d)
print(loc_date)
cn = pytz.country_timezones['CN']
print(cn)
