"""
你需要转换或者输出使用二进制，八进制或十六进制表示的整数
"""
a = 1234
print(bin(a))
print(oct(a))
print(hex(a))
print(format(a, 'b'))
print(format(a, 'o'))
print(format(a, 'x'))
print(int('10011010010', 2))
print(int('4d2', 16))