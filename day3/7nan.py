"""
你想创建或测试正无穷、负无穷或NaN(非数字)的浮点数。
"""
import math

a = float('inf')
b = float('-inf')
c = float('nan')
print(a)
print(b)
print(c)
print(math.isinf(a))
print(math.isinf(b))
print(math.isnan(c))