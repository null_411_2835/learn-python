"""
你进入时间机器，突然发现你正在做小学家庭作业，并涉及到分数计算问题。 或者你可能需要写代码去计算在你的木工工厂中的测量值。
"""

from fractions import Fraction
a = Fraction(3, 4)
b = Fraction(5, 6)
print(a)
print(b)
print(a*b)
print(a+b)