"""
你想从一个序列中随机抽取若干元素，或者想生成几个随机数。
"""

import random
val = [1, 3, 4, 5, 9]
print(random.choice(val))
print(random.sample(val, 3))
print(val)
print(random.shuffle(val))
print(val)
print(random.randint(1, 30))
print(random.random())
print(random.randbytes(20))
print(random.getrandbits(10))