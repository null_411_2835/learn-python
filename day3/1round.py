"""
想对浮点数执行指定精度的舍入运算。
"""


a = 1.76321
b = 65535
print(a)
print(round(a, 3))
print(round(a, 2))
print(round(a, 1))
print(round(a, -1))
print(round(b, -1))
print(round(b, -2))