"""
你需要执行简单的时间转换，比如天到秒，小时到分钟等的转换
"""
from datetime import timedelta, datetime

a = timedelta(days=1, hours=3)
print(a.days)
print(a.seconds)
b = datetime(2010, 10, 28)
print(a+timedelta(days=2))
print(b+timedelta(minutes=20))