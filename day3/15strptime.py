"""
你的应用程序接受字符串格式的输入，但是你想将它们转换为 datetime 对象以便在上面执行非字符串操作
"""
from datetime import datetime
text = '2012-09-20'
y = datetime.strptime(text, '%Y-%m-%d')
print(y)
z = datetime.now()
print(z)
diff = z-y
print(diff)
nice_z = datetime.strftime(z, '%A %B %d, %Y')
print(nice_z)