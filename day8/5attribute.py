"""
你想封装类的实例上面的“私有”数据，但是Python语言并没有访问控制。
"""

class A:
    def __init__(self):
        self._internal = 0
        self.public = 1

    def public_method(self):
        """
        a public method
        """
        pass

    def _internal_method(self):
        """

        :return:
        """
        pass