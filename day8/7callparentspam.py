"""
你想在子类中调用父类的某个已经被覆盖的方法。
"""


class A:
    def spam(self):
        print('a.spam')


class B:
    def spam(self):
        print('b.spam')
        super().spam()


b = B()
b.spam