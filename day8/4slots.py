"""
你的程序要创建大量(可能上百万)的对象，导致占用很大的内存
"""


class Date:
    __slots__ = ['year', 'month', 'day']

    def __init__(self, year, month, day):
        self.year=year
        self.month=month
        self.day=day

