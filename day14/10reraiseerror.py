"""
你在一个 except 块中捕获了一个异常，现在想重新抛出它。
"""


def example():
    try:
        int('N/A')
    except ValueError:
        print('do not work')
        raise


example()