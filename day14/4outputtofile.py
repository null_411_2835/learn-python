"""
你希望将单元测试的输出写到到某个文件中去，而不是打印到标准输出。
"""
import sys
import unittest


def main(out=sys.stderr, verbosity=2):
        loader = unittest.TestLoader()
        suite = loader.loadTestsFromModule(sys.modules[__name__])
        unittest.TextTestRunner(out, verbosity=verbosity).run(suite)


if __name__ == '__main__':
    with open('testing.out', 'w') as f:
       main(f)