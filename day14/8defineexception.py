"""
在你构建的应用程序中，你想将底层异常包装成自定义的异常。
"""


class NetWorkError(Exception):
    pass


class HostnameError(NetWorkError):
    pass


class TimeoutError(NetWorkError):
    pass


class ProtocolError(NetWorkError):
    pass
