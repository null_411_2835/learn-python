"""
你希望自己的程序能生成警告信息（比如废弃特性或使用问题）。
"""
import warnings


def func(x, y, logfile=None, debug=False):
    if logfile is not None:
        warnings.warn('logfile argument deprecated', DeprecationWarning)


func(1, 2)
