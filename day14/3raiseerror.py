"""
你想写个测试用例来准确的判断某个异常是否被抛出。
"""

import unittest

def parse_int(s):
    return int(s)


class TestConversion(unittest.TestCase):
    def test_bad_int(self):
        self.assertRaises(ValueError, parse_int, 'N/A')