"""
你想在单元测试中忽略或标记某些测试会按照预期运行失败。
"""

import unittest
import os
import platform


class Tests(unittest.TestCase):
    def test_0(self):
        self.assertTrue(True)

    @unittest.skip('skipped test')
    def test_1(self):
        self.fail('should have failed!')

    @unittest.skipIf(os.name=='posix', 'Not supported on unix')
    def test_2(self):
        import winreg

    @unittest.skipUnless(platform.system()=='darwin', 'mac specific test')
    def test_3(self):
        self.assertTrue(True)

    @unittest.expectedFailure
    def test_4(self):
        self.assertEqual(2+2, 5)


if __name__ == '__main__':
    unittest.main()
