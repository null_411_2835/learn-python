"""
你想捕获一个异常后抛出另外一个不同的异常，同时还得在异常回溯中保留两个异常的信息。
"""


def example():
    try:
        int('N/A')
    except ValueError as e:
        raise RuntimeError('a parsing error occurred') from e


example()