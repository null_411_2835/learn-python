"""
你想在迭代一个序列的同时跟踪正在被处理的元素索引。
"""

my_list = ['a', 'b', 'c']
for idx, val in enumerate(my_list, 1):
    print(idx, val)