"""
你想反方向迭代一个序列
"""

a = [1, 2, 3, 4]
for i in reversed(a):
    print(i)


class Countdown:
    def __init__(self, start):
        self.start = start

    def __iter__(self):
        n = self.start
        while n > 0:
            yield n
            n-=1

    def __reversed__(self):
        n = 1
        while n<=self.start:
            yield n
            n += 1


for rr in Countdown(5):
    print(rr)

for rr in reversed(Countdown(5)):
    print(rr)