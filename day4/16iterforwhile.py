"""
你在代码中使用 while 循环来迭代处理数据，因为它需要调用某个函数或者和一般迭代模式不同的测试条件。 能不能用迭代器来重写这个循环呢？
"""
import sys
f = open(r'D:\code\gitee\LearnPython\LICENSE')
for chunk in iter(lambda: f.read(3), b''):
    n = sys.stdout.write(chunk)

