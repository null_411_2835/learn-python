"""
你想遍历一个可迭代对象，但是它开始的某些元素你并不感兴趣，想跳过它们。
"""
from itertools import dropwhile, islice

# with open(r'D:\code\gitee\LearnPython\LICENSE') as f:
#     for line in f:
#         print(line, end='')
items = ['a', 'b', 'c', 1, 2, 3]


with open(r'D:\code\gitee\LearnPython\LICENSE') as f:
    for line in dropwhile(lambda line: not line.startswith("#"), f):
        print(line, end='')


for x in islice(items, 3, None):
    print(x)


