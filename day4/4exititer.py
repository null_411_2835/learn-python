"""
你想构建一个能支持迭代操作的自定义对象，并希望找到一个能实现迭代协议的简单方法。
"""


class Node:
    def __init__(self, value):
        self.__value = value
        self.__children = []

    def __repr__(self):
        return 'Node({!r})'.format(self.__value)

    def add_child(self, node):
        self.__children.append(node)

    def depth_first(self):
        yield self
        for c in self:
            yield from c.depth_first()


if __name__ == '__main__':
    root = Node(0)
    child1 = Node(1)
    child2 = Node(2)
    root.add_child(child1)
    root.add_child(child2)
    child1.add_child(Node(3))
    child1.add_child(Node(4))
    child2.add_child(Node(5))

    for ch in root.depth_first():
        print(ch)