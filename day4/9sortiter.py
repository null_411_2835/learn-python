"""
你想迭代遍历一个集合中元素的所有可能的排列或组合
"""
from itertools import permutations, combinations, combinations_with_replacement

items = ['a', 'b', 'c']
for p in permutations(items):
    print(p)

for p in permutations(items, 2):
    print(p)

for p in combinations(items, 1):
    print(p)

for p in combinations_with_replacement(items, 3):
    print(p)