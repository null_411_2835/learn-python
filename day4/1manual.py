"""
你想遍历一个可迭代对象中的所有元素，但是却不想使用for循环。
"""


def manual_iter():
    with open('/etc/passwd') as f:
        try:
            while True:
                line = next(f)
                print(line,end='')
        except StopIteration:
            pass
