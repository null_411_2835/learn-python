"""
你想读写JSON(JavaScript Object Notation)编码格式的数据。
"""
import json
from pprint import pprint
from urllib.request import urlopen

data = {
    'name' : 'ACME',
    'shares' : 100,
    'price' : 542.23
}
json_str = json.dumps(data)
print(json_str)
pprint(json_str)
data = json.loads(json_str)
print(data)

with open('data.json', 'w') as f:
    json.dump(data, f)
with open('data.json', 'r') as f:
    da = json.load(f)
    pprint(da)

u = urlopen('http://www.baidu.com')
# todo 解码失败
resp = json.loads(u.decode('utf-8'))
print(resp)
