"""
你需要使用Base64格式解码或编码二进制数据。
"""
import base64
s = b'wennuan'
print(s)
a = base64.b64encode(s)
print(a)
b = base64.b64decode(a)
print(b)