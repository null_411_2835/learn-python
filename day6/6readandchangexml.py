"""
你想读取一个XML文档，对它最一些修改，然后将结果写回XML文档。
"""
from xml.etree.ElementTree import parse, Element
doc = parse('pred.xml')
root = doc.getroot()
print(root)
root.remove(root.find('sri'))
root.remove(root.find('cr'))
# root.getchildren().index(root.find('nm'))
print(doc)
e = Element('spam')
e.text = 'this is bill'
root.insert(2, e)
doc.write('newpred.xml', xml_declaration=True)