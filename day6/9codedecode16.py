"""
你想将一个十六进制字符串解码成一个字节字符串或者将一个字节字符串编码成一个十六进制字符串。
"""
import binascii
import base64
s = b'hello'
print(s)
h = binascii.b2a_hex(s)
print(h)
deh = binascii.a2b_hex(h)
print(deh)
b64hex = base64.b16encode(s)
print(b64hex)
decode64 = base64.b16decode(b64hex)
print(decode64)